<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Scores;
use App\Repository\ScoresRepository;

class GameController extends AbstractController
{
    #[Route('/game', name: 'app_game')]
    public function index(): Response
    {
        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
        ]);
    }

    #[Route('/game/save-scores', name: 'game_save_scores')]
    public function saveScoreAction(Request $request, ScoresRepository $ScoresRepository ): JsonResponse
    {


        $score = $request->request->get('scores');
        // Valider le score (par exemple, vérifier qu'il est numérique et positif)
        if (!is_numeric($score) || $score < 0) {
            return new JsonResponse(['success' => false, 'message' => 'Score invalide ']);
        }
        // Récupérer l'utilisateur connecté
        $user = $this->getUser();

        if (!$user) {
            return new JsonResponse(['success' => false, 'message' => 'Utilisateur non connecté']);
        }

        // Enregistrer le score dans la base de données avec le nom d'utilisateur

        $score_save = new Scores;
        $score_save->setUser($user);
        $score_save->setScores($score);


        $ScoresRepository->save($score_save, true);

        return new JsonResponse(['success' => true, 'message' => 'Score enregistré']);
        
    }
}