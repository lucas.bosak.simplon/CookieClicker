<?php

namespace App\Controller;

use App\Entity\Scores;
use App\Form\ScoresType;
use App\Repository\ScoresRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_main')]
    public function index(ScoresRepository $scoresRepository): Response
    {
        $scores = $scoresRepository->findBy([], ['Scores' => 'DESC']);
        
        return $this->render('scores/show.html.twig', [
            'scores' => $scores,
        ]);
    }
}