let container = document.querySelector('.container');
let btn = document.querySelector('.start_btn');
let scoreContainer = document.querySelector('.score');
let timeContainer = document.querySelector('.time');
btn.onclick = function() {
  btn.style.display = 'none';
  let score = 0;
  let time = 10;
  container.innerHTML = "";
  let target = document.createElement('img');
  target.id = "target";
  target.src = "cookie.png";
  container.appendChild(target);

  target.onclick = function() {
    score += 1;
    scoreContainer.innerHTML = `Score : ${score}`;
  };
//temp
  let interval = setInterval(showTarget, 1000);

  function showTarget() {

    time -= 1;

    timeContainer.innerHTML = `Temps restant : ${time}`;

    if (time === 0) {
      clearInterval(interval);
      container.innerHTML = `<h1>Le jeu est terminé.<br>Votre scores: ${score}</h1>`;
      btn.style.display = 'block';
    }
  };
};

