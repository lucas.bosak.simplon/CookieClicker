let container = document.querySelector('.container');
let btn = document.querySelector('.start_btn');
let scoreContainer = document.querySelector('.score');
let timeContainer = document.querySelector('.time');
let score = 0; // déclarer la variable score en dehors de la fonction btn.onclick

btn.onclick = function() {
  let score = 0
  btn.style.display = 'none';
  let time = 10;
  container.innerHTML = "";
  let target = document.createElement('img');
  target.id = "target";
  target.src = "assets/img/Cookie.png";
  container.appendChild(target);

  target.onclick = function() {
    score += 1;
    scoreContainer.innerHTML = `Cookie : ${score}`;
  };

  //temp
  let interval = setInterval(showTarget, 1000);

  function showTarget() {
    time -= 1;

    timeContainer.innerHTML = `Temps restant : ${time}`;

    if (time === 0) {
      clearInterval(interval);
      container.innerHTML = `<h1>Le jeu est terminé.<br>Votre score: <sc>${score}</sc></h1>`;
      btn.style.display = 'block';

      $.ajax({
        url: saveScoresUrl,
        method: 'POST',
        data: {
          scores: score 
        },
        dataType: 'json',
        success: function(response) {
          console.log(response.message); // Afficher la réponse de la requête
        },
        error: function(xhr, status, error) {
          console.error(error); // Afficher l'erreur s'il y en a une
          alert("Une erreur s'est produite lors de l'enregistrement du score.");
        }
      });
    }
  }

}